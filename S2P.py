#i = 1
#array = []
#with open("C:\\Users\\Ser\\Documents\\tesis\\Genetica_poblaciones\\AFLP\\info_global_25-11-16\\STRUCTURE\EVANO\\K4.indfile", "r") as f:
#	for line in f:
#		if line == '\n':
#			print i
#			output = open("K4r%s.Q" %i, "w")
#			for row in array:
#				output.write(row)
#			output.close()
#			i = i + 1
#			array = []
#		else:
#			#print line
#			array.append(line)
			


# this file gets data from Kx.indfile and converts them into separate Q-matrices per run
# this will check all files in a directory and loop through all of them

# let i: k, j: run
import os
import re
import sys

def check_indfile(filename):
    if re.match(r"K\d+.indfile", filename):
        return 1
    else:
        return 0

def extract_k(filename):
    pattern = r"K(\d+)\.indfile"

    match = re.match(pattern, filename)

    if(match):
        return match.group(1)
    else:
        return

def to_qmatrix(filename, indfile_dir, out_dir):
    # remove directory portion of filename
    filename_nodir = filename.lstrip(indfile_dir+"/")

    # retrieve k to make output file name
    # output file format: K<i>r<j>.Q
    i = extract_k(filename_nodir)

    pong_filemap = out_dir + "/pong_filemap"
    # iterate through all lines of file
    # when newline is encountered, count that as a new run
    with open(filename) as f:
        j = 1
        rows = []
        tab = '\t'
        newline = '\n'

        for line in f:
            if line == "\n":
                out_file = f"K{i}r{j}.Q"

                print(f"Writing to out_file")

                output = open(out_dir+"/"+out_file, "w")
                for row in rows:
                        output.write(row)
                output.close()
                rows = []

                # after writing Kxrx.Q, append this to a pong_filemap
                filemap = f"k{i}r{j}{tab}{i}{tab}{out_file}{newline}"
                print(f"Writing {filemap} to pong_filemap")

                with open(pong_filemap, "a") as pf:
                    pf.write(filemap)

                # update run counter
                j += 1

            else:
                rows.append(line)

def main(indfile_dir, out_dir):
    # check if indfile_dir exists
    if os.path.exists(indfile_dir):
        file_list = os.listdir(indfile_dir)
        indfiles = []
    else:
        print("Directory containing indfiles does not exist")
        return

    # create out_dir if it still does not yet exist
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # retrieve only files with the .indfile extension
    for file in file_list:
        # check if file is an indfile
        if(check_indfile(file)):
            indfiles.append(indfile_dir+"/"+file)

    print(indfiles)

    pong_filemap = out_dir + "/pong_filemap"

    # first open a pong_filemap and initialize it as an empty file
    with open(pong_filemap, "w") as f:
        pass

    for file in indfiles:
        print(f"Converting {file} to q-matrix files...")
        to_qmatrix(file, indfile_dir, out_dir)
        print("Conversion successful")

    # iterate through all files of indfiles list
    return

if __name__ == "__main__":
    if len(sys.argv) == 3:
        indfile_dir = sys.argv[1]
        out_dir = sys.argv[2]
        valid = True
    elif len(sys.argv) == 1:
        indfile_dir = "data"
        out_dir = "out"
        valid = True
    else:
        print("Please provide both the paths to the directory of indfiles and the intended output directory")
        valid = False

    if(valid):
        main(indfile_dir, out_dir)
