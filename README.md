# S2P

This is a fork of the original [S2P repository](https://github.com/Sergio-Sclovich/S2P.git)

I have added several improvements so that all .indfiles inside a certain directory are automatically processed. This also automatically writes a file `pong_filemap` which contains the paths to the .Q files that S2P.py generates.

## What this does
This converts STRUCTURE (https://web.stanford.edu/group/pritchardlab/structure.html) indfiles 
to 
PONG (https://github.com/ramachandran-lab/pong) input files

Usage:

1)Create a directory containing the indfiles (for example, indfiles)

2)Copy the output of STRUCTURE into the folder. Assuming structureHarvester was performed, the file names should follow the format Ki.indfile where i is the k (K1.indfile, K7.indfile, etc.)

3)Run the script using `python3 S2P.py path/to/indfiles`. If the path is not defined, S2P.py will automatically look for a `data` directory which should hopefully contain the indfiles.
